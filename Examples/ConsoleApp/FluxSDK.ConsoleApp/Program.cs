﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Flux.ConsoleApp.Commands;
using Flux.ConsoleApp.Enums;
using Flux.SDK;


namespace Flux.ConsoleApp
{
    static class Program
    {
        static void Main()
        {

            Console.Title = "FluxSDK Scratch Console";
            string command = "";
            //JUST INIT AND LOGIN
            string projectID = "ayXAG056pdQL79zWZ"; //FOR HACKATHON ONLY
            Command.Init(clientId: "7a37b499-ab6f-4196-86c8-4da6ca659edb", clientVersion: "dev");
            Command.Login(clientSecret: "14832047-7969-409f-b7c0-38ee1498e77b", serverUrl: "localhost:8080");
            while (!Command.logins) {  
                //just waitin' for login to proceed....             
            }
            List<String> keys = Command.GetKeyList(projectId: projectID);
            /*
            List<String> geoJson  = new List<string>();
           // int[] InitKeys = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            //string[] returnJson[] =  new string[] { };
            for (int i=0; i  <InitKeys.Length; i++)
            {
                geoJson.Add(Command.GetKeyAsJson(projectId: projectID, keyId: keys[InitKeys[i]]));
            }
            Console.WriteLine(geoJson.Count);
            */
            //actually, here, we are going to stay connected and listening, and accept and log future changes 
            bool exit = false;//   turns off console interface
            while (!exit) 
            {
                string[] inputArgs = ReadFromConsole("> ");
                //string[] inputArgs = Console.ReadLine();
                command = inputArgs[0];
                int k = Convert.ToInt32(inputArgs[1]);
                string data = "";
                for (int j = 2; j < inputArgs.Length; j++) {
                    data += inputArgs[j];
                }
                if (command == "push")
                {
                    //get location value from scratch file or unity 
                    Command.SetKey(projectID, keys[k], data);
                }
                if (command == "pull")
                {
                    //get location value from scratch file or unity 
                    Command.GetKey(projectID, keys[k], data);
                    Console.WriteLine(data);
                }
            }
        }

        private static string[] ReadFromConsole(string promptMessage = "")
        {
            string inputStr;

            do
            {
                Console.Write(Environment.NewLine + "fluxConsole" + promptMessage);
                inputStr = Console.ReadLine();
            } while (string.IsNullOrWhiteSpace(inputStr));

            //prepare input string for parsing parameters with spaces 
            var regex = new Regex(@"(.*?)""([^""]*)""(.*)");
            Match match = regex.Match(inputStr);
            while (match.Success)
            {
                //if have empty parameter, for example: ""
                if (string.IsNullOrEmpty(match.Groups[2].Value))
                    inputStr = string.Format("{0}0{1}{2}", match.Groups[1].Value, '+', match.Groups[3].Value);
                else
                    inputStr = string.Format("{0}{1}{2}", match.Groups[1].Value, match.Groups[2].Value.Replace(" ", "+"), match.Groups[3].Value);

                match = regex.Match(inputStr);
            }

            //cleaning parameters and parse parameters with spaces 
            List<string> resultList = new List<string>();

            string[] strings = inputStr.Split(' ');
            foreach (string value in strings)
            {
                if(string.IsNullOrWhiteSpace(value))
                    continue;

                string cleanValue = value.Trim().Replace("0+", string.Empty).Replace("+", " ");

                resultList.Add(cleanValue);
            }

            return resultList.ToArray();
        }
    }
}
