From the Console Demo, wev'e built a Flux push/pull command line app for the AEC hackathon project. It is being invoked and control from Unity, and would be a standalone for Windows tethered scene manipulation.

The bones of this tool can be used to flesh out better Flux integration for a variety of use cases